package com.example.com.andware.test;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class SecondActivity extends Activity {

	private Button bt1,bt2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		bt1 = (Button) findViewById(R.id.button1);
		bt1.setOnClickListener(listener);
		bt2 = (Button) findViewById(R.id.button2);
		bt2.setOnClickListener(listener);
	}
	
	private OnClickListener listener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.button1:
				MainActivity.openActivity(SecondActivity.this);
				break;
			case R.id.button2:
				MainActivity.closeActivity(SecondActivity.this);
				break;
			default:
				break;
			}
		}
	};
	
}
