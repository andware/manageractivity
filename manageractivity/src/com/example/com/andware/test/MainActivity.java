
package com.example.com.andware.test;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends BaseActivity {

	
	private Button bt1,bt2;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		bt1 = (Button) findViewById(R.id.button1);
		bt1.setOnClickListener(listener);
		bt2 = (Button) findViewById(R.id.button2);
		bt2.setOnClickListener(listener);
		startActivity(new Intent().setClass(MainActivity.this, SecondActivity.class));
	}

	private OnClickListener listener = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			switch (v.getId()) {
			case R.id.button1:
				openActivity(MainActivity.this);
				break;
			case R.id.button2:
				closeActivity(MainActivity.this);
				break;
			default:
				break;
			}
		}
	};
	 
	
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		
	}

}
