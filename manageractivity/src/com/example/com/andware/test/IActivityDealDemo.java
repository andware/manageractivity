package com.example.com.andware.test;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

public class IActivityDealDemo implements IActivityDeal {

	@Override
	public void close(Context context) {
		// TODO Auto-generated method stub
		if ( context instanceof Activity ) {
			Activity activity = (Activity)context;
			activity.finish();
		} else {
			Log.i("LOG","do nothing!");
		}
	}

	@Override
	public int doWhat( Object object) {
		// TODO Auto-generated method stub
		final Integer i;
		if (object instanceof Integer) {
			i = (Integer) object;
			DoWhatRunnable run = new DoWhatRunnable();
			run.setI(i);
			Thread thread = new Thread(run);
			thread.run();
			return run.getI();
		} else {
			Log.i("LOG", "do nothing!");
			return 0;
		}

	}
	private class DoWhatRunnable implements Runnable {

		private int i;
		
		@Override
		public void run() {
			// TODO Auto-generated method stub
			i++;
			Log.i("LOG","����i:"+i);
		}
		
		public int getI () {
			return i;
		}
		
		public void setI ( int i ) {
			this.i = i;
		}
		
	}
	@Override
	public void doWhat() {
		// TODO Auto-generated method stub
		
	}
}
