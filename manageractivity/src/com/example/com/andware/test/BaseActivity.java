
/**
 * 
 * @author ssj
 * 
 * Activity操作封装类，此套模式可以使得activity操作变得更加方便
 * 
 * 
 * 
 * **/

package com.example.com.andware.test;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

public class BaseActivity extends Activity{

	public static final String ACTION_OPEN = "com.andware.openActivity"; 
	
	public static final String ACTION_CLOSE = "com.andware.closeActivity";
	
	private int i = -2;
	
	private IActivityDeal iActivityDeal;
	
	public static void openActivity ( Context context ) {
		if ( ! ( context instanceof BaseActivity ) && context instanceof Activity ) {
			Activity activity = (Activity)context;
			activity.finish();
		}
		context.startActivity(new Intent()
		.setClass(context, MainActivity.class)
		.setAction(ACTION_OPEN)
		.putExtra("DEAL",1 )
		.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
	}
	
	public static void closeActivity ( Context context ) {
		if ( ! ( context instanceof BaseActivity ) && context instanceof Activity ) {
			Activity activity = (Activity)context;
			activity.finish();
		}
		context.startActivity(new Intent()
		.setClass(context, MainActivity.class)
		.setAction(ACTION_CLOSE)
		.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP));
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		i = 0;
		iActivityDeal = new IActivityDealDemo();
	}
	
	/**
	 * 
	 * @author ssj
	 * 该模式在跳转activty的时候跳过该Activity的onCreate方法，只要此Activity不消失，该Activity中初始化的数据就不会丢失。所以我们只需要复写
	 * onNewIntent获取正常跳转的action状态做些数据更新或者添加操作
	 * @exception 该例子只做了2中状态，大家可以更具此例的结构进行扩展
	 * **/
	protected void onNewIntent(Intent intent) {
		// TODO Auto-generated method stub
		String action = intent.getAction();
		if ((intent.getFlags() & Intent.FLAG_ACTIVITY_LAUNCHED_FROM_HISTORY) != 0) {
			super.onNewIntent(intent);
		} else if ( action.equals(Intent.ACTION_VIEW) || action.equals(ACTION_CLOSE) ) {
			//关闭app
			iActivityDeal.close(BaseActivity.this);
		} else if (action.equals(Intent.ACTION_VIEW)||ACTION_OPEN.equals(action)) {
			//跳转后的处理
			if ( intent.getIntExtra("DEAL", -1) != -1 && intent.getIntExtra("DEAL", -1) == 1 ) {
				//跳转转到该activity的处理(注意使用该方式的intent跳转后可以跳过该Activty的oncreate方法)
				//数据更新操作
				//如果i输出结果为1则这套结论是正确的，如果i输出结果为-1则是说明跳过了onCreate方法，但是第一次调用的onCreate的方法的状态没有被保存，如果输出结果为1则说明以上结论正确。
				i = iActivityDeal.doWhat(new Integer(i));
			}
		} else {
			super.onNewIntent(intent);
		}
	}
	
}
